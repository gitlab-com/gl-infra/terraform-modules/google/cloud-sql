# GitLab.com Cloud SQL Terraform Module

## What is this?

This module provisions a Google Cloud SQL instance. It is based on the [terraform-google-sql-db/postgresql](https://github.com/terraform-google-modules/terraform-google-sql-db/tree/master/modules/postgresql) module.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Usage

```terraform
module "cloud-sql" {
 source  = "ops.gitlab.net/gitlab-com/cloud-sql/google"
 version = "X.Y.Z"

 database_version = "DATABASE_VERSION"
 name             = "DATABASE_NAME"
 project          = "GCP_PROJECT_ID"
 vpc              = "NETWORK_LINK"
}
```

Once you have loaded this module, you can reference the outputs to access the instance.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 5.7.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 3.0.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 5.7.0 |
| <a name="provider_null"></a> [null](#provider\_null) | >= 3.0.0 |
| <a name="provider_random"></a> [random](#provider\_random) | >= 3.0.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_compute_global_address.service-network-private-ip](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_project_iam_custom_role.db-role](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_custom_role) | resource |
| [google_project_iam_member.exports-db](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.exports](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_networking_connection.private_vpc_connection](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_networking_connection) | resource |
| [google_sql_database.additional_databases](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database) | resource |
| [google_sql_database.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database) | resource |
| [google_sql_database_instance.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database_instance) | resource |
| [google_sql_database_instance.replicas](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database_instance) | resource |
| [google_sql_user.additional_users](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_user) | resource |
| [google_sql_user.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_user) | resource |
| [google_storage_bucket.exports](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket_iam_policy.exports](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_policy) | resource |
| [null_resource.delay](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [random_id.google_sql_name_suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [random_id.service-account-id-suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [random_password.user_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [google_iam_policy.bucket-policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/iam_policy) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_activation_policy"></a> [activation\_policy](#input\_activation\_policy) | The activation policy for the master instance.Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`. | `string` | `"ALWAYS"` | no |
| <a name="input_additional_bucket_permissions"></a> [additional\_bucket\_permissions](#input\_additional\_bucket\_permissions) | A list of users and service accounts to give access to the export bucket. | `list(string)` | `[]` | no |
| <a name="input_additional_databases"></a> [additional\_databases](#input\_additional\_databases) | A list of databases to be created in your cluster. | <pre>list(object({<br/>    name      = string<br/>    charset   = optional(string)<br/>    collation = optional(string)<br/>  }))</pre> | `[]` | no |
| <a name="input_additional_users"></a> [additional\_users](#input\_additional\_users) | A map of users to be created in your cluster. | <pre>map(object({<br/>    name     = string<br/>    password = optional(string)<br/>    type     = optional(string, "BUILT_IN")<br/>  }))</pre> | `{}` | no |
| <a name="input_availability_type"></a> [availability\_type](#input\_availability\_type) | The availability type for the master instance.This is only used to set up high availability for the PostgreSQL instance. Can be either `ZONAL` or `REGIONAL`. | `string` | `"ZONAL"` | no |
| <a name="input_backup_configuration"></a> [backup\_configuration](#input\_backup\_configuration) | The backup configuration block of the Cloud SQL resources<br/>This argument will be passed through the master instance directly.<br/><br/>See [more details](https://www.terraform.io/docs/providers/google/r/sql_database_instance.html). | <pre>object({<br/>    binary_log_enabled             = optional(bool)<br/>    enabled                        = optional(bool, false)<br/>    start_time                     = optional(string)<br/>    point_in_time_recovery_enabled = optional(bool, false)<br/>    backup_retention_settings = optional(object({<br/>      retained_backups = optional(number, 7)<br/>    }), { retained_backups = 7 })<br/>  })</pre> | `{}` | no |
| <a name="input_create_bucket"></a> [create\_bucket](#input\_create\_bucket) | Whether to create a new GCS bucket (optionally) specified in `var.exports_bucket`. | `bool` | `true` | no |
| <a name="input_create_timeout"></a> [create\_timeout](#input\_create\_timeout) | The optional timeout that is applied to limit long database creates. | `string` | `"10m"` | no |
| <a name="input_database_flags"></a> [database\_flags](#input\_database\_flags) | The database flags for the master instance. See [more details](https://cloud.google.com/sql/docs/mysql/flags). | <pre>list(object({<br/>    name  = string<br/>    value = string<br/>  }))</pre> | `[]` | no |
| <a name="input_database_version"></a> [database\_version](#input\_database\_version) | The database version to use. | `string` | n/a | yes |
| <a name="input_db_charset"></a> [db\_charset](#input\_db\_charset) | The charset for the default database. | `string` | `""` | no |
| <a name="input_db_collation"></a> [db\_collation](#input\_db\_collation) | The collation for the default database. Example: `en_US.UTF8`. | `string` | `""` | no |
| <a name="input_db_name"></a> [db\_name](#input\_db\_name) | The name of the default database to create. | `string` | `"default"` | no |
| <a name="input_delete_timeout"></a> [delete\_timeout](#input\_delete\_timeout) | The optional timeout that is applied to limit long database deletes. | `string` | `"10m"` | no |
| <a name="input_disk_autoresize"></a> [disk\_autoresize](#input\_disk\_autoresize) | Configuration to increase storage size. | `bool` | `false` | no |
| <a name="input_disk_size"></a> [disk\_size](#input\_disk\_size) | The disk size for the master instance. | `number` | `10` | no |
| <a name="input_disk_type"></a> [disk\_type](#input\_disk\_type) | The disk type for the master instance. | `string` | `"PD_SSD"` | no |
| <a name="input_enable_exports"></a> [enable\_exports](#input\_enable\_exports) | Whether to enable GCP service account and GCS bucket for automated exports via `gcloud sql export sql ...`. | `bool` | `false` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Whether to enable this module or skip creation of resources. | `bool` | `true` | no |
| <a name="input_exports_bucket"></a> [exports\_bucket](#input\_exports\_bucket) | Name to use for GCS bucket where exported data files will be stored. If not set, defaults to `NAME-PROJECT-cloudsql-exports`. | `string` | `null` | no |
| <a name="input_exports_service_account"></a> [exports\_service\_account](#input\_exports\_service\_account) | GCP service account with permissions to access database and write export files to the EXPORTS\_BUCKET. If not set, defaults to `NAME-PROJECT-cloudsql-exports`. | `string` | `null` | no |
| <a name="input_global_address_name"></a> [global\_address\_name](#input\_global\_address\_name) | The name of the global network address block. | `string` | `"private-ip"` | no |
| <a name="input_ip_configuration"></a> [ip\_configuration](#input\_ip\_configuration) | The IP configuration for the master instances. | <pre>object({<br/>    ipv4_enabled    = optional(bool, true)<br/>    private_network = optional(string)<br/>    ssl_mode        = optional(string)<br/>    authorized_networks = optional(list(<br/>      object({<br/>        expiration_time = optional(string)<br/>        name            = string<br/>        value           = string<br/>      })<br/>    ), [])<br/>  })</pre> | `{}` | no |
| <a name="input_maintenance_window_day"></a> [maintenance\_window\_day](#input\_maintenance\_window\_day) | The day of week (1-7) for the master instance maintenance. | `number` | `1` | no |
| <a name="input_maintenance_window_hour"></a> [maintenance\_window\_hour](#input\_maintenance\_window\_hour) | The hour of day (0-23) maintenance window for the master instance maintenance. | `number` | `23` | no |
| <a name="input_maintenance_window_update_track"></a> [maintenance\_window\_update\_track](#input\_maintenance\_window\_update\_track) | The update track of maintenance window for the master instance maintenance. Can be either `canary` or `stable`. | `string` | `"canary"` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the Cloud SQL resources. | `string` | n/a | yes |
| <a name="input_pricing_plan"></a> [pricing\_plan](#input\_pricing\_plan) | The pricing plan for the master instance. | `string` | `"PER_USE"` | no |
| <a name="input_project"></a> [project](#input\_project) | The project ID in which to manage the Cloud SQL resources. | `string` | n/a | yes |
| <a name="input_random_instance_name"></a> [random\_instance\_name](#input\_random\_instance\_name) | Sets random suffix at the end of the Cloud SQL resource name | `bool` | `true` | no |
| <a name="input_read_replica_activation_policy"></a> [read\_replica\_activation\_policy](#input\_read\_replica\_activation\_policy) | The activation policy for the read replica instances. Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`. | `string` | `"ALWAYS"` | no |
| <a name="input_read_replica_availability_type"></a> [read\_replica\_availability\_type](#input\_read\_replica\_availability\_type) | The availability type for the read replica instances.This is only used to set up high availability for the PostgreSQL instances. Can be either `ZONAL` or `REGIONAL`. | `string` | `"ZONAL"` | no |
| <a name="input_read_replica_configuration"></a> [read\_replica\_configuration](#input\_read\_replica\_configuration) | The replica configuration for use in read replica. | <pre>object({<br/>    ca_certificate            = optional(string)<br/>    client_certificate        = optional(string)<br/>    client_key                = optional(string)<br/>    connect_retry_interval    = optional(number)<br/>    dump_file_path            = optional(string)<br/>    master_heartbeat_period   = optional(number)<br/>    password                  = optional(string)<br/>    ssl_cipher                = optional(string)<br/>    username                  = optional(string)<br/>    verify_server_certificate = optional(bool)<br/>  })</pre> | `{}` | no |
| <a name="input_read_replica_database_flags"></a> [read\_replica\_database\_flags](#input\_read\_replica\_database\_flags) | The database flags for the read replica instances. See [more details](https://cloud.google.com/sql/docs/mysql/flags). | <pre>list(object({<br/>    name  = string<br/>    value = string<br/>  }))</pre> | `[]` | no |
| <a name="input_read_replica_disk_autoresize"></a> [read\_replica\_disk\_autoresize](#input\_read\_replica\_disk\_autoresize) | Configuration to increase storage size. | `bool` | `true` | no |
| <a name="input_read_replica_disk_size"></a> [read\_replica\_disk\_size](#input\_read\_replica\_disk\_size) | The disk size for the read replica instances. | `number` | `10` | no |
| <a name="input_read_replica_disk_type"></a> [read\_replica\_disk\_type](#input\_read\_replica\_disk\_type) | The disk type for the read replica instances. | `string` | `"PD_SSD"` | no |
| <a name="input_read_replica_ip_configuration"></a> [read\_replica\_ip\_configuration](#input\_read\_replica\_ip\_configuration) | The IP configuration for the read replica instances. | <pre>object({<br/>    ipv4_enabled    = optional(bool, true)<br/>    private_network = optional(string)<br/>    ssl_mode        = optional(string)<br/>    authorized_networks = optional(list(<br/>      object({<br/>        expiration_time = optional(string)<br/>        name            = string<br/>        value           = string<br/>      })<br/>    ), [])<br/>  })</pre> | `{}` | no |
| <a name="input_read_replica_name_suffix"></a> [read\_replica\_name\_suffix](#input\_read\_replica\_name\_suffix) | The optional suffix to add to the read instance name. | `string` | `""` | no |
| <a name="input_read_replica_pricing_plan"></a> [read\_replica\_pricing\_plan](#input\_read\_replica\_pricing\_plan) | The pricing plan for the read replica instances. | `string` | `"PER_USE"` | no |
| <a name="input_read_replica_size"></a> [read\_replica\_size](#input\_read\_replica\_size) | The size of read replicas. | `number` | `0` | no |
| <a name="input_read_replica_tier"></a> [read\_replica\_tier](#input\_read\_replica\_tier) | The tier for the read replica instances. | `string` | `""` | no |
| <a name="input_read_replica_user_labels"></a> [read\_replica\_user\_labels](#input\_read\_replica\_user\_labels) | The key/value labels for the read replica instances. | `map(string)` | `{}` | no |
| <a name="input_read_replica_zones"></a> [read\_replica\_zones](#input\_read\_replica\_zones) | The zones for the read replica instancess, it should be something like: `['a','b','c']`. Given zones are used rotationally for creating read replicas. | `list(string)` | `[]` | no |
| <a name="input_region"></a> [region](#input\_region) | The region of the Cloud SQL resources. | `string` | `"us-central1"` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | The tier for the master instance. | `string` | `"db-f1-micro"` | no |
| <a name="input_update_timeout"></a> [update\_timeout](#input\_update\_timeout) | The optional timeout that is applied to limit long database updates. | `string` | `"10m"` | no |
| <a name="input_user_labels"></a> [user\_labels](#input\_user\_labels) | The key/value labels for the master instances. | `map(string)` | `{}` | no |
| <a name="input_user_name"></a> [user\_name](#input\_user\_name) | The name of the default user. | `string` | `"default"` | no |
| <a name="input_user_password"></a> [user\_password](#input\_user\_password) | The password for the default user. If not set, a random one will be generated and available in the generated\_user\_password output variable. | `string` | `""` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | The network link for the private IP address block. | `string` | n/a | yes |
| <a name="input_vpc_peering_enabled"></a> [vpc\_peering\_enabled](#input\_vpc\_peering\_enabled) | Whether to create VPC peering resources. | `bool` | `true` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | Optional. The zone for the master instance, it should be something like: `a`, `c`. | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_generated_user_password"></a> [generated\_user\_password](#output\_generated\_user\_password) | The auto generated default user password if not input password was provided |
| <a name="output_instance_address"></a> [instance\_address](#output\_instance\_address) | The IPv4 addesses assigned for the master instance |
| <a name="output_instance_connection_name"></a> [instance\_connection\_name](#output\_instance\_connection\_name) | The connection name of the master instance to be used in connection strings |
| <a name="output_instance_first_ip_address"></a> [instance\_first\_ip\_address](#output\_instance\_first\_ip\_address) | The first IPv4 address of the addresses assigned. |
| <a name="output_instance_name"></a> [instance\_name](#output\_instance\_name) | The instance name for the master instance |
| <a name="output_instance_self_link"></a> [instance\_self\_link](#output\_instance\_self\_link) | The URI of the master instance |
| <a name="output_instance_server_ca_cert"></a> [instance\_server\_ca\_cert](#output\_instance\_server\_ca\_cert) | The CA certificate information used to connect to the SQL instance via SSL |
| <a name="output_instance_service_account_email_address"></a> [instance\_service\_account\_email\_address](#output\_instance\_service\_account\_email\_address) | The service account email address assigned to the master instance |
| <a name="output_private_address"></a> [private\_address](#output\_private\_address) | The private IP address assigned for the master instance |
| <a name="output_read_replica_instance_names"></a> [read\_replica\_instance\_names](#output\_read\_replica\_instance\_names) | The instance names for the read replica instances |
| <a name="output_replicas_instance_connection_names"></a> [replicas\_instance\_connection\_names](#output\_replicas\_instance\_connection\_names) | The connection names of the replica instances to be used in connection strings |
| <a name="output_replicas_instance_ip_addresses"></a> [replicas\_instance\_ip\_addresses](#output\_replicas\_instance\_ip\_addresses) | The IPv4 addresses assigned for the replica instances |
| <a name="output_replicas_instance_self_links"></a> [replicas\_instance\_self\_links](#output\_replicas\_instance\_self\_links) | The URIs of the replica instances |
| <a name="output_replicas_instance_server_ca_certs"></a> [replicas\_instance\_server\_ca\_certs](#output\_replicas\_instance\_server\_ca\_certs) | The CA certificates information used to connect to the replica instances via SSL |
| <a name="output_replicas_instance_service_account_email_addresses"></a> [replicas\_instance\_service\_account\_email\_addresses](#output\_replicas\_instance\_service\_account\_email\_addresses) | The service account email addresses assigned to the replica instances |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
