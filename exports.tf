resource "google_project_iam_custom_role" "db-role" {
  count = local.enable_exports ? 1 : 0

  permissions = [
    "cloudsql.instances.connect",
    "cloudsql.instances.export",
    "cloudsql.instances.get",
    "cloudsql.instances.list",
  ]
  project = var.project
  role_id = "${replace(var.project, "-", "_")}_${replace(var.name, "-", "_")}_exports_db_role"
  title   = "CloudSQL DB access role (${var.project}/${var.name})"
}

resource "random_id" "service-account-id-suffix" {
  count = local.enable_exports ? 1 : 0

  byte_length = 2
}

resource "google_service_account" "exports" {
  count = local.enable_exports ? 1 : 0

  account_id  = var.exports_service_account != null ? "${substr(var.exports_service_account, 0, 25)}-${random_id.service-account-id-suffix[0].hex}" : "${substr(local.default_name, 0, 25)}-${random_id.service-account-id-suffix[0].hex}"
  description = "Service account used for exporting data from DATABASE to bucket GCS_BUCKET"
  project     = var.project
}

// GCP service accounts are eventually consistent, so we add a delay before adding the
// role binding.
// TODO: Create a script/loop that polls the API to test for service account creation
resource "null_resource" "delay" {
  count = local.enable_exports ? 1 : 0

  provisioner "local-exec" {
    command = "sleep 30"
  }

  triggers = {
    "before" = google_service_account.exports[0].id
  }
}

resource "google_project_iam_member" "exports-db" {
  count = local.enable_exports ? 1 : 0

  project = var.project
  role    = google_project_iam_custom_role.db-role[0].id
  member  = var.enable_exports ? "serviceAccount:${google_service_account.exports[0].email}" : "serviceAccount:FAKE_SA_EMAIL" // Kludgey hack to workaround conditional resources

  # Supported in GCP provider >= 3.x
  #condition {
  #  title      = "Restrict access to specific cloudsql instance"
  #  expression = "resource name = cloudsql instance"
  #}

  depends_on = [null_resource.delay]
}

resource "google_storage_bucket" "exports" {
  count = local.enable_exports && var.create_bucket ? 1 : 0

  location      = var.region
  name          = var.exports_bucket != null ? var.exports_bucket : "${var.name}-${var.project}-cloudsql-exports"
  project       = var.project
  storage_class = "REGIONAL"

  uniform_bucket_level_access = true

  lifecycle_rule {
    condition {
      age = 7
    }
    action {
      type = "Delete"
    }
  }

  versioning {
    enabled = true
  }
}

data "google_iam_policy" "bucket-policy" {
  count = local.enable_exports && var.create_bucket ? 1 : 0

  binding {
    role = "roles/storage.objectAdmin"
    members = concat(
      [
        "serviceAccount:${google_service_account.exports[0].email}",
        "serviceAccount:${google_sql_database_instance.default[0].service_account_email_address}"
      ],
      var.additional_bucket_permissions
    )
  }
}

resource "google_storage_bucket_iam_policy" "exports" {
  count = local.enable_exports && var.create_bucket ? 1 : 0

  bucket      = google_storage_bucket.exports[0].name
  policy_data = data.google_iam_policy.bucket-policy[0].policy_data
}
