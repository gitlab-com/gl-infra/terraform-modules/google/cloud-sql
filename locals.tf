locals {
  primary_zone = var.zone

  zones_enabled = length(var.read_replica_zones) > 0
  mod_by        = local.zones_enabled ? length(var.read_replica_zones) : 1
  zones         = local.zones_enabled ? var.read_replica_zones : [local.primary_zone]

  default_name = "${var.project}-${var.name}-exports"

  enable_exports = var.enabled && var.enable_exports

  master_instance_name = var.enabled && var.random_instance_name ? "${var.name}-${random_id.google_sql_name_suffix[0].hex}" : var.name
}
