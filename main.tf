resource "random_id" "google_sql_name_suffix" {
  count = var.enabled && var.random_instance_name ? 1 : 0

  byte_length = 2
}

resource "google_sql_database_instance" "default" {
  count = var.enabled ? 1 : 0

  project          = var.project
  name             = local.master_instance_name
  database_version = var.database_version
  region           = var.region

  settings {
    tier              = var.tier
    activation_policy = var.activation_policy
    availability_type = var.availability_type

    backup_configuration {
      binary_log_enabled             = var.backup_configuration.binary_log_enabled
      enabled                        = var.backup_configuration.enabled
      start_time                     = var.backup_configuration.start_time
      point_in_time_recovery_enabled = var.backup_configuration.point_in_time_recovery_enabled
      dynamic "backup_retention_settings" {
        for_each = var.backup_configuration.backup_retention_settings != null ? [1] : []
        content {
          retained_backups = var.backup_configuration.backup_retention_settings.retained_backups
        }
      }
    }

    ip_configuration {
      ipv4_enabled    = var.ip_configuration.ipv4_enabled
      private_network = var.ip_configuration.private_network
      ssl_mode        = var.ip_configuration.ssl_mode

      dynamic "authorized_networks" {
        for_each = { for network in var.ip_configuration.authorized_networks : network.name => network }

        content {
          expiration_time = authorized_networks.value.expiration_time
          name            = authorized_networks.value.name
          value           = authorized_networks.value.value
        }
      }
    }

    disk_autoresize = var.disk_autoresize
    disk_size       = var.disk_autoresize ? null : var.disk_size
    disk_type       = var.disk_type
    pricing_plan    = var.pricing_plan

    dynamic "database_flags" {
      for_each = var.database_flags

      content {
        name  = database_flags.value.name
        value = database_flags.value.value
      }
    }

    user_labels = var.user_labels

    dynamic "location_preference" {
      for_each = var.zone != "" ? [var.zone] : []

      content {
        zone = "${var.region}-${location_preference.value}"
      }
    }

    maintenance_window {
      day          = var.maintenance_window_day
      hour         = var.maintenance_window_hour
      update_track = var.maintenance_window_update_track
    }
  }

  depends_on = [google_service_networking_connection.private_vpc_connection]

  lifecycle {
    ignore_changes = [
      settings[0].disk_size
    ]
  }

  timeouts {
    create = var.create_timeout
    update = var.update_timeout
    delete = var.delete_timeout
  }
}

resource "google_sql_database" "default" {
  count = var.enabled ? 1 : 0

  name      = var.db_name
  project   = var.project
  instance  = google_sql_database_instance.default[0].name
  charset   = var.db_charset
  collation = var.db_collation

  depends_on = [google_sql_database_instance.default]
}

resource "google_sql_database" "additional_databases" {
  for_each = var.enabled ? { for db in var.additional_databases : db.name => db } : {}

  project   = var.project
  name      = each.key
  charset   = each.value.charset
  collation = each.value.collation
  instance  = google_sql_database_instance.default[0].name

  depends_on = [google_sql_database_instance.default]
}

resource "random_password" "user_password" {
  count = var.enabled ? 1 : 0

  length = 16
  keepers = {
    instance = google_sql_database_instance.default[0].name
  }

  depends_on = [google_sql_database_instance.default]
}

resource "google_sql_user" "default" {
  count = var.enabled ? 1 : 0

  name     = var.user_name
  project  = var.project
  instance = google_sql_database_instance.default[0].name
  password = var.user_password == "" ? random_password.user_password[0].result : var.user_password

  depends_on = [google_sql_database_instance.default]
}

resource "google_sql_user" "additional_users" {
  for_each = var.enabled ? var.additional_users : {}

  project  = var.project
  name     = each.value.name
  password = each.value.type == "BUILT_IN" ? (each.value.password != null ? each.value.password : random_password.user_password[0].result) : null
  type     = each.value.type
  instance = google_sql_database_instance.default[0].name

  depends_on = [google_sql_database_instance.default]
}

resource "google_compute_global_address" "service-network-private-ip" {
  count = var.vpc_peering_enabled ? 1 : 0

  name          = var.global_address_name
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = var.vpc
  project       = var.project
}

resource "google_service_networking_connection" "private_vpc_connection" {
  count = var.vpc_peering_enabled ? 1 : 0

  network                 = var.vpc
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.service-network-private-ip[0].name]
}
