resource "google_sql_database_instance" "replicas" {
  count = var.read_replica_size

  project              = var.project
  name                 = "${var.name}-replica${var.read_replica_name_suffix}${count.index}"
  database_version     = var.database_version
  region               = var.region
  master_instance_name = google_sql_database_instance.default[0].name

  replica_configuration {
    ca_certificate            = var.read_replica_configuration.ca_certificate
    client_certificate        = var.read_replica_configuration.client_certificate
    client_key                = var.read_replica_configuration.client_key
    connect_retry_interval    = var.read_replica_configuration.connect_retry_interval
    dump_file_path            = var.read_replica_configuration.dump_file_path
    failover_target           = false
    master_heartbeat_period   = var.read_replica_configuration.master_heartbeat_period
    password                  = var.read_replica_configuration.password
    ssl_cipher                = var.read_replica_configuration.ssl_cipher
    username                  = var.read_replica_configuration.username
    verify_server_certificate = var.read_replica_configuration.verify_server_certificate
  }

  settings {
    tier              = var.read_replica_tier
    activation_policy = var.read_replica_activation_policy
    availability_type = var.read_replica_availability_type

    ip_configuration {
      ipv4_enabled    = var.ip_configuration.ipv4_enabled
      private_network = var.ip_configuration.private_network
      ssl_mode        = var.ip_configuration.ssl_mode

      dynamic "authorized_networks" {
        for_each = { for network in var.read_replica_ip_configuration.authorized_networks : network.name => network }

        content {
          expiration_time = authorized_networks.value.expiration_time
          name            = authorized_networks.value.name
          value           = authorized_networks.value.value
        }
      }
    }

    disk_autoresize = var.read_replica_disk_autoresize
    disk_size       = var.read_replica_disk_size
    disk_type       = var.read_replica_disk_type
    pricing_plan    = var.read_replica_pricing_plan
    user_labels     = var.read_replica_user_labels

    dynamic "database_flags" {
      for_each = var.read_replica_database_flags

      content {
        name  = database_flags.value.name
        value = database_flags.value.value
      }
    }

    location_preference {
      zone = length(local.zones) == 0 ? "" : "${var.region}-${local.zones[count.index % local.mod_by]}"
    }
  }

  depends_on = [google_sql_database_instance.default]

  lifecycle {
    ignore_changes = [
      settings[0].disk_size
    ]
  }

  timeouts {
    create = var.create_timeout
    update = var.update_timeout
    delete = var.delete_timeout
  }
}
