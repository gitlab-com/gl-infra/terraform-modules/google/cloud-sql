variable "project" {
  type        = string
  description = "The project ID in which to manage the Cloud SQL resources."
}

variable "name" {
  type        = string
  description = "The name of the Cloud SQL resources."
}

// required
variable "database_version" {
  type        = string
  description = "The database version to use."
}

// required
variable "region" {
  type        = string
  description = "The region of the Cloud SQL resources."
  default     = "us-central1"
}

variable "tier" {
  type        = string
  description = "The tier for the master instance."
  default     = "db-f1-micro"
}

variable "zone" {
  type        = string
  description = "Optional. The zone for the master instance, it should be something like: `a`, `c`."
  default     = ""
}

variable "enabled" {
  type        = bool
  description = "Whether to enable this module or skip creation of resources."
  default     = true
}

variable "vpc_peering_enabled" {
  type        = bool
  description = "Whether to create VPC peering resources."
  default     = true
}

variable "activation_policy" {
  type        = string
  description = "The activation policy for the master instance.Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`."
  default     = "ALWAYS"
}

variable "availability_type" {
  type        = string
  description = "The availability type for the master instance.This is only used to set up high availability for the PostgreSQL instance. Can be either `ZONAL` or `REGIONAL`."
  default     = "ZONAL"
}

variable "disk_autoresize" {
  type        = bool
  description = "Configuration to increase storage size."
  default     = false
}

variable "disk_size" {
  type        = number
  description = "The disk size for the master instance."
  default     = 10
}

variable "disk_type" {
  type        = string
  description = "The disk type for the master instance."
  default     = "PD_SSD"
}

variable "pricing_plan" {
  type        = string
  description = "The pricing plan for the master instance."
  default     = "PER_USE"
}

variable "maintenance_window_day" {
  type        = number
  description = "The day of week (1-7) for the master instance maintenance."
  default     = 1
}

variable "maintenance_window_hour" {
  type        = number
  description = "The hour of day (0-23) maintenance window for the master instance maintenance."
  default     = 23
}

variable "maintenance_window_update_track" {
  type        = string
  description = "The update track of maintenance window for the master instance maintenance. Can be either `canary` or `stable`."
  default     = "canary"
}

variable "database_flags" {
  type = list(object({
    name  = string
    value = string
  }))
  description = "The database flags for the master instance. See [more details](https://cloud.google.com/sql/docs/mysql/flags)."
  default     = []
}

variable "user_labels" {
  type        = map(string)
  description = "The key/value labels for the master instances."
  default     = {}
}

variable "backup_configuration" {
  type = object({
    binary_log_enabled             = optional(bool)
    enabled                        = optional(bool, false)
    start_time                     = optional(string)
    point_in_time_recovery_enabled = optional(bool, false)
    backup_retention_settings = optional(object({
      retained_backups = optional(number, 7)
    }), { retained_backups = 7 })
  })
  description = <<EOF
The backup configuration block of the Cloud SQL resources
This argument will be passed through the master instance directly.

See [more details](https://www.terraform.io/docs/providers/google/r/sql_database_instance.html).
EOF
  default     = {}
}

variable "ip_configuration" {
  type = object({
    ipv4_enabled    = optional(bool, true)
    private_network = optional(string)
    ssl_mode        = optional(string)
    authorized_networks = optional(list(
      object({
        expiration_time = optional(string)
        name            = string
        value           = string
      })
    ), [])
  })
  description = "The IP configuration for the master instances."
  default     = {}
}

variable "read_replica_size" {
  type        = number
  description = "The size of read replicas."
  default     = 0
}

variable "read_replica_name_suffix" {
  type        = string
  description = "The optional suffix to add to the read instance name."
  default     = ""
}

variable "read_replica_tier" {
  type        = string
  description = "The tier for the read replica instances."
  default     = ""
}

variable "read_replica_zones" {
  type        = list(string)
  description = "The zones for the read replica instancess, it should be something like: `['a','b','c']`. Given zones are used rotationally for creating read replicas."
  default     = []
}

variable "read_replica_activation_policy" {
  type        = string
  description = "The activation policy for the read replica instances. Can be either `ALWAYS`, `NEVER` or `ON_DEMAND`."
  default     = "ALWAYS"
}

variable "read_replica_availability_type" {
  type        = string
  description = "The availability type for the read replica instances.This is only used to set up high availability for the PostgreSQL instances. Can be either `ZONAL` or `REGIONAL`."
  default     = "ZONAL"
}


variable "read_replica_disk_autoresize" {
  type        = bool
  description = "Configuration to increase storage size."
  default     = true
}

variable "read_replica_disk_size" {
  type        = number
  description = "The disk size for the read replica instances."
  default     = 10
}

variable "read_replica_disk_type" {
  type        = string
  description = "The disk type for the read replica instances."
  default     = "PD_SSD"
}

variable "read_replica_pricing_plan" {
  type        = string
  description = "The pricing plan for the read replica instances."
  default     = "PER_USE"
}

variable "read_replica_database_flags" {
  type = list(object({
    name  = string
    value = string
  }))
  description = "The database flags for the read replica instances. See [more details](https://cloud.google.com/sql/docs/mysql/flags)."
  default     = []
}

variable "read_replica_configuration" {
  type = object({
    ca_certificate            = optional(string)
    client_certificate        = optional(string)
    client_key                = optional(string)
    connect_retry_interval    = optional(number)
    dump_file_path            = optional(string)
    master_heartbeat_period   = optional(number)
    password                  = optional(string)
    ssl_cipher                = optional(string)
    username                  = optional(string)
    verify_server_certificate = optional(bool)
  })
  description = "The replica configuration for use in read replica."
  default     = {}
}

variable "read_replica_user_labels" {
  type        = map(string)
  description = "The key/value labels for the read replica instances."
  default     = {}
}


variable "read_replica_ip_configuration" {
  type = object({
    ipv4_enabled    = optional(bool, true)
    private_network = optional(string)
    ssl_mode        = optional(string)
    authorized_networks = optional(list(
      object({
        expiration_time = optional(string)
        name            = string
        value           = string
      })
    ), [])
  })
  description = "The IP configuration for the read replica instances."
  default     = {}
}

variable "db_name" {
  type        = string
  description = "The name of the default database to create."
  default     = "default"
}

variable "db_charset" {
  type        = string
  description = "The charset for the default database."
  default     = ""
}

variable "db_collation" {
  type        = string
  description = "The collation for the default database. Example: `en_US.UTF8`."
  default     = ""
}

variable "additional_databases" {
  type = list(object({
    name      = string
    charset   = optional(string)
    collation = optional(string)
  }))
  description = "A list of databases to be created in your cluster."
  default     = []
}

variable "user_name" {
  type        = string
  description = "The name of the default user."
  default     = "default"
}

variable "user_password" {
  type        = string
  description = "The password for the default user. If not set, a random one will be generated and available in the generated_user_password output variable."
  default     = ""
}

variable "additional_users" {
  type = map(object({
    name     = string
    password = optional(string)
    type     = optional(string, "BUILT_IN")
  }))
  default     = {}
  description = "A map of users to be created in your cluster."
}

variable "create_timeout" {
  type        = string
  description = "The optional timeout that is applied to limit long database creates."
  default     = "10m"
}

variable "update_timeout" {
  type        = string
  description = "The optional timeout that is applied to limit long database updates."
  default     = "10m"
}

variable "delete_timeout" {
  type        = string
  description = "The optional timeout that is applied to limit long database deletes."
  default     = "10m"
}

variable "global_address_name" {
  type        = string
  description = "The name of the global network address block."
  default     = "private-ip"
}

variable "vpc" {
  type        = string
  description = "The network link for the private IP address block."
}

variable "enable_exports" {
  type        = bool
  description = "Whether to enable GCP service account and GCS bucket for automated exports via `gcloud sql export sql ...`."
  default     = false
}

variable "create_bucket" {
  type        = bool
  description = "Whether to create a new GCS bucket (optionally) specified in `var.exports_bucket`."
  default     = true
}

variable "exports_bucket" {
  type        = string
  description = "Name to use for GCS bucket where exported data files will be stored. If not set, defaults to `NAME-PROJECT-cloudsql-exports`."
  default     = null
}

variable "exports_service_account" {
  type        = string
  description = "GCP service account with permissions to access database and write export files to the EXPORTS_BUCKET. If not set, defaults to `NAME-PROJECT-cloudsql-exports`."
  default     = null
}

variable "additional_bucket_permissions" {
  type        = list(string)
  description = "A list of users and service accounts to give access to the export bucket."
  default     = []
}

variable "random_instance_name" {
  type        = bool
  description = "Sets random suffix at the end of the Cloud SQL resource name"
  default     = true
}
